﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorLib
{
	public class Calculator
	{
		/// <summary>
		/// Метод для складывания двух чисел
		/// </summary>
		/// <param name="first">первый операнд</param>
		/// <param name="second">второй операнд</param>
		/// <returns>сумма двух операндов</returns>
		public double Sum(double first, double second)
		{
			return first + second;
		}
		
		/// <summary>
		/// Метод для умножения двух чисел
		/// </summary>
		/// <param name="a">первый операнд</param>
		/// <param name="b">второй операнд</param>
		/// <returns>Произведение двух операндов</returns>
		public double Mult(double a, double b)
		{
			return a * b;
        }

        /// <summary>
        /// Метод для нахождения разности двух чисел
        /// </summary>
        /// <param name="a">первый операнд</param>
        /// <param name="b">второй операнд</param>
        /// <returns>Произведение двух операндов</returns>
        public double Sub(double a, double b)
        {
            return a - b;
        }

        /// <summary>
        /// Метод для нахождения синуса числа
        /// </summary>
        /// <returns>The sin.</returns>
        /// <param name="a">The alpha component.</param>
        public double Sin(double a)
        {
            return Math.Sin(a);
        }

        /// <summary>
        /// Метод для нахождения косинуса числа
        /// </summary>
        /// <returns>The sin.</returns>
        /// <param name="a">The alpha component.</param>
        public double Cos(double a)
        {
            return Math.Cos(a);
        }

        /// <summary>
        /// Метод для нахождения тангенса числа
        /// </summary>
        /// <returns>The sin.</returns>
        /// <param name="a">The alpha component.</param>
        public double Tan(double a)
        {
            return Math.Tan(a);
        }
        
        /// <summary>
        /// Метод для деления двух чисел
        /// </summary>
        /// <param name="a">первый операнд</param>
        /// <param name="b">второй операнд</param>
        /// <returns>Деление на число</returns>
        /// <exception cref="DivideByZeroException"></exception>
        public double Divide(double a, double b)
        {
	        if (b == 0)
		        throw new DivideByZeroException("Нельзя делить на ноль");
	        return a / b;
        }

        /// <summary>
        /// Метод для возведения первого числа в степень второго
        /// </summary>
        /// <param name="a">Число которое нужно возвести в степень</param>
        /// <param name="b">Степень в которую нужно возвести число</param>
        /// <returns>Число a в степени b</returns>
        public double Pow(double a, double b)
        {
            return Math.Pow(a, b);
        }


        /// <summary>
        /// Метод для нахождения остатка от деления первого числа на второе
        /// </summary>
        /// <param name="first">первый операнд</param>
        /// <param name="second">второй операнд</param>
        /// <returns>Остаток от деления двух операндов</returns>
        public double Mod(double first, double second)
        {
            if (second == 0)
                throw new DivideByZeroException("Нельзя делить на ноль");
            return first % second;
        }
        /// <summary>
        /// Метод для получения логарифма из первого числа по основанию другого 
        /// </summary>
        /// <param name="a">число</param>
        /// <param name="b">основание</param>
        /// <returns>логариф числа а по основанию b</returns>
        public double Log(double a,double b)
		{
			if (b <= 0||b==1)
				throw new ArgumentException("Основание логарифма должно быть >0 и !=1 !");
			if(a<=0)
				throw new ArgumentException("Число должно быть больше нуля!");
			return Math.Log(a, b);
		}

		/// <summary>
		/// Метод взятия корня из первого числа степени второго числа
		/// </summary>
		/// <param name="a">первый операнд</param>
		/// <param name="b">второй операнд</param>
		/// <returns>корень из первого числа степени второго числа</returns>
		public double Sqrt(double a, double b)
		{
			if (b == 0)
				return 1;

			return Math.Pow(a, 1 / b);
		}

        /// <summary>
        /// Метод, вычисляющий факториал числа
        /// </summary>
        /// <param name="a">Число, для которго будет вычисляться факториал</param>
        /// <returns>Факториал числа</returns>
        public int Fact(int a)
        {
            if (a < 0)
                return (a != -1) ? a * -1 * Fact(a + 1) : -1;
            else
                return ((a != 1) && (a != 0)) ? a * Fact(a - 1) : 1;
        }


    }
}
