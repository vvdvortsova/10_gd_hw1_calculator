﻿/*
 Проект: Калькулятор
 Цель: написать программу с использованием системы контроля версий (СКВ) и системы
 управления проектом.
 Команда: 10

 */
using System;
using CalculatorLib;

namespace MainApp
{
	class Program
	{
        /// <summary>
        /// Метод для корректного чтения целого числа
        /// </summary>
        /// <param name="mes">Сообщение о входных данных</param>
        /// <returns></returns>
        public static int ReadInt(string mes)
        {
            Console.Write(mes);
            int ans;
            while (!int.TryParse(Console.ReadLine(), out ans))
            {
                Console.WriteLine("Некорректное число");
                Console.Write(mes);
            }
            return ans;

        }


        /// <summary>
        /// Метод для корректного чтения дробного числа
        /// </summary>
        /// <param name="mes">Сообщение о входных данных</param>
        /// <returns></returns>
        public static double ReadDouble(string mes)
        {
            Console.Write(mes);
            double ans;
            while (!double.TryParse(Console.ReadLine(), out ans))
            {
                Console.WriteLine("Некорректное число");
                Console.Write(mes);
            }
            return ans;

        }

        /// <summary>
        /// Метод для вывода приветственного сообщения 
        /// </summary>
        public static void Hello()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            string hello = "--------------------------------------------------\n" +
                "Добро пожаловать в наше приложение!\n" +
                "В данном приложение доступны следующие операторы:\n" +
                "1) + для сложения двух чисел\n" +
                "2) - для вычитания числа из другого числа\n" +
                "3) / для деления числа на другое число\n" +
                "4) * для умножения двух чисел\n" +
                "5) % для получения остатка от деления одного числа на другое\n" +
                "6) pow для возведение одного числа в степень другого\n" +
                "7) sqrt для получения корня одного числа степени другого\n" +
                "8) log для получения логарифма из первого числа по основанию другого \n" +
                "9) ! для вычисления факториала числа\n" +
                "10) sin для вычисления синуса от числа\n" +
                "11) cos для вычисления косинуса от числа\n" +
                "12) tan для вычисления тангенса от числа\n" +
                "13) 0 для завершения работы программы\n" +
                "--------------------------------------------------\n";
            Console.WriteLine(hello);
            Console.ForegroundColor = ConsoleColor.White;
        }


        static void Main(string[] args)
        {
            Hello();
            Console.Write("Знак: ");
            string op = Console.ReadLine();
            double x_db, y_db;
            int x_int;
            Calculator calculator = new Calculator();
            op = op.Replace(" ", "");
            while (op != "0")
            {
                switch (op)
                {
                    case "+":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        Console.WriteLine(calculator.Sum(x_db, y_db));
                        break;
                    case "-":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        Console.WriteLine(calculator.Sub(x_db, y_db));
                        break;
                    case "*":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        Console.WriteLine(calculator.Mult(x_db, y_db));
                        break;
                    case "/":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        try
                        {
                            Console.WriteLine(calculator.Divide(x_db, y_db));
                        }
                        catch(DivideByZeroException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case "pow":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        if (calculator.Pow(x_db, y_db) == double.NaN)
                        {
                            Console.WriteLine("Число невозможно возвести в данную степень ");
                        }
                        Console.WriteLine(calculator.Pow(x_db, y_db));
                        break;
                    case "sqrt":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        Console.WriteLine(calculator.Sqrt(x_db, y_db));
                        break;
                    case "log":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        try
                        {
                            Console.WriteLine(calculator.Log(x_db, y_db));
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case "!":
                        x_int = ReadInt("x = ");
                        if (x_int > 25)
                        {
                            Console.WriteLine("Слишком большое значения для расчёта факториала числа");
                        }
                        else
                        {
                            Console.WriteLine(calculator.Fact(x_int));
                        }
                        break;
                    case "sin":
                        x_db = ReadDouble("x = ");
                        Console.WriteLine(calculator.Sin(x_db));
                        break;
                    case "cos":
                        x_db = ReadDouble("x = ");
                        Console.WriteLine(calculator.Cos(x_db));
                        break;
                    case "tan":
                        x_db = ReadDouble("x = ");
                        Console.WriteLine(calculator.Tan(x_db));
                        break;
                    case "%":
                        x_db = ReadDouble("x = ");
                        y_db = ReadDouble("y = ");
                        Console.WriteLine(calculator.Mod(x_db, y_db));
                        break;
                    default:
                        Console.WriteLine("Неверно указан знак операции");
                        break;
                }
                Console.Write("Знак: ");
                op = Console.ReadLine();
                op = op.Replace(" ", "");
            }
        }
    }
}
